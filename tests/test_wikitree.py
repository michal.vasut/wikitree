import json

import pytest
from src.dataclasses import WTEvent, WTImage, WTPerson, WTPrivacy, WTRelation, WTRelType
from wikitree.src.errors import WTProfileIdentificationException
from wikitree.src.wikitree import Wikitree

PERSON_DATA = """
{'user_id': '3636', 'person': {'Id': 3636, 'Name': 'Adams-35', 'FirstName': 'John', 'MiddleName': '', 'LastNameAtBirth': 'Adams', 'LastNameCurrent': 'Adams', 'Nicknames': '', '
LastNameOther': '', 'RealName': 'John', 'Prefix': 'Deacon', 'Suffix': '', 'BirthLocation': 'Braintree, Suffolk County, Massachusetts Bay Colony', 'DeathLocation': 'Braintree, S
uffolk County, Province of Massachusetts Bay', 'Gender': 'Male', 'BirthDate': '1691-02-08', 'DeathDate': '1761-05-25', 'BirthDateDecade': '1690s', 'DeathDateDecade': '1760s', '
Photo': 'Adams-35.jpg', 'IsLiving': 0, 'HasChildren': 1, 'NoChildren': 1, 'Privacy': 60, 'IsPerson': 1, 'Touched': '20201127030120', 'ShortName': 'John Adams', 'BirthNamePrivat
e': 'John Adams', 'LongNamePrivate': 'John Adams', 'LongName': 'John  Adams', 'BirthName': 'John Adams', 'Manager': 10844696, 'DataStatus': {'Spouse': 'known', 'Father': '20',
'Mother': '20', 'FirstName': 'certain', 'MiddleName': '', 'LastNameCurrent': 'certain', 'RealName': 'certain', 'LastNameOther': '', 'Gender': '', 'BirthDate': 'certain', 'Birth
Location': 'certain', 'DeathLocation': 'certain', 'Photo': None, 'Prefix': 'certain', 'Suffix': '', 'Nicknames': '', 'DeathDate': 'certain', 'LastNameAtBirth': '', 'ColloquialN
ame': '', 'Email': '', 'Bio': ''}, 

'Privacy_IsPrivate': False, 'Privacy_IsPublic': False, 'Privacy_IsOpen': True, 'Privacy_IsAtLeastPublic': True, 'Privacy_IsSemiPrivate': False, 'Privacy_IsSemiPrivateBio': False, 

'PhotoData': {
    'path': '/photo.php/e/e3/Adams-35.jpg', 
    'url': '/photo.php/thumb/e/e3/Adams-35.jpg/75px-Adams-35.jpg', 
    'file': '75px-Adams-35.jpg', 
    'dir': '/photo.php/thumb/e/e3', 
    'width': 75, 
    'height': 50, 
    'orig_width': '700', 
    'orig_height': '466'},
 
 'Father': 3640, 'Mother': 3641, 
 
 'Parents': 
 
 {'3640': {'Id': 3640,
'Name': 'Adams-38', 'FirstName': 'Joseph', 'MiddleName': '', 'LastNameAtBirth': 'Adams', 'LastNameCurrent': 'Adams', 'Nicknames': '', 'LastNameOther': '', 'RealName': 'Joseph',
 'Prefix': 'Deacon', 'Suffix': 'Jr.', 'BirthLocation': 'Braintree, Suffolk, Massachusetts Bay Colony', 'DeathLocation': 'Braintree, Suffolk, Province of Massachusetts Bay', 'Ge
nder': 'Male', 'BirthDate': '1654-12-24', 'DeathDate': '1736-02-12', 'BirthDateDecade': '1650s', 'DeathDateDecade': '1730s', 'Photo': '', 'IsLiving': 0, 'HasChildren': 1, 'NoCh
ildren': 0, 'Privacy': 60, 'IsPerson': 1, 'Touched': '20200719152629', 'ShortName': 'Joseph Adams Jr.', 'BirthNamePrivate': 'Joseph Adams Jr.', 'LongNamePrivate': 'Joseph Adams
 Jr.', 'LongName': 'Joseph  Adams Jr.', 'BirthName': 'Joseph Adams Jr.', 'Manager': 10701125, 'DataStatus': {'Spouse': 'known', 'Father': '20', 'Mother': None, 'FirstName': '',
 'MiddleName': 'blank', 'LastNameCurrent': '', 'RealName': '', 'LastNameOther': '', 'Gender': '', 'BirthDate': '', 'BirthLocation': '', 'DeathLocation': 'certain', 'Photo': Non
e, 'Prefix': '', 'Nicknames': '', 'DeathDate': 'certain', 'Suffix': '', 'LastNameAtBirth': '', 'ColloquialName': ''},
 
 'Privacy_IsPrivate': False, 'Privacy_IsPublic': False, 'Privacy_IsOpen': True, 'Privacy_IsAtLeastPublic': True, 'Privacy_IsSemiPrivate': False, 'Privacy_IsSemiPrivateBio': False, 
 
 'Father': 3642, 
 'Mother': 3643}, 
 
 '3641': {'Id': 3641, '
Name': 'Bass-1', 'FirstName': 'Hannah', 'MiddleName': '', 'LastNameAtBirth': 'Bass', 'LastNameCurrent': 'Adams', 'Nicknames': '', 'LastNameOther': '', 'RealName': 'Hannah', 'Pr
efix': '', 'Suffix': '', 'BirthLocation': 'Braintree, Quincy, Suffolk, Massachusetts Bay Colony', 'DeathLocation': 'Braintree, Quincy, Suffolk, Province of Massachusetts Bay',
'Gender': 'Female', 'BirthDate': '1667-06-22', 'DeathDate': '1705-10-24', 'BirthDateDecade': '1660s', 'DeathDateDecade': '1700s', 'Photo': '', 'IsLiving': 0, 'HasChildren': 1,
'NoChildren': 0, 'Privacy': 60, 'IsPerson': 1, 'Touched': '20201104030930', 'ShortName': 'Hannah (Bass) Adams', 'BirthNamePrivate': 'Hannah Bass', 'LongNamePrivate': 'Hannah (B
ass) Adams', 'LongName': 'Hannah  (Bass) Adams', 'BirthName': 'Hannah Bass', 'Manager': 10701125, 'DataStatus': {'Spouse': 'known', 'Father': '20', 'Mother': '20', 'FirstName':
 'certain', 'MiddleName': '', 'LastNameCurrent': 'certain', 'RealName': 'certain', 'LastNameOther': '', 'Gender': '', 'BirthDate': 'certain', 'DeathDate': 'certain', 'BirthLoca
tion': 'certain', 'DeathLocation': 'certain', 'Photo': None, 'Prefix': '', 'Suffix': '', 'Nicknames': '', 'LastNameAtBirth': '', 'ColloquialName': ''}, 'Privacy_IsPrivate': Fal
se, 'Privacy_IsPublic': False, 'Privacy_IsOpen': True, 'Privacy_IsAtLeastPublic': True, 'Privacy_IsSemiPrivate': False, 'Privacy_IsSemiPrivateBio': False, 'Father': 3660, 'Moth
er': 3661}}}, 'status': 0}
"""


class TestWikitree:
    def setup_class(self):
        self.wt = Wikitree()

        dummy = WTPerson()

        dummy.id_ = 3636
        dummy.key = "Adams-35"
        dummy.first_name = ""
        dummy.last_name_at_birth = ""
        dummy.last_name_current = ""
        dummy.last_name_other = ""
        dummy.nicknames = []
        dummy.real_name = ""
        dummy.prefix = ""
        dummy.suffix = ""

        dummy.birth = WTEvent("", None, "")
        dummy.death = WTEvent("", None, "")

        dummy.photo = WTImage()
        dummy.thumbnail = WTImage()

        dummy.is_living = False
        dummy.gender = "male"

        dummy.has_children = True
        dummy.no_children = True

        dummy.short_name = ""
        dummy.long_name_private = ""
        dummy.long_name = ""
        dummy.birth_name = ""

        dummy.mother = None
        dummy.father = None
        dummy.spouses = []
        dummy.siblings = []

        dummy.manager = None
        dummy.touched = None
        dummy.privacy = WTPrivacy(60)

        self.dummy_person = dummy

    def test_get_person_none(self):
        with pytest.raises(WTProfileIdentificationException):
            self.wt.get_person()

    def test_get_person_key(self):
        assert self.wt.get_person(key="Adams-35") == WTPerson()

    def test_get_person_key(self):
        assert self.wt.get_person(id="3636") == WTPerson()
