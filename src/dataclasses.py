from enum import Enum


class WTEvent:
    def __init__(self, location="", date=None, decade=""):
        self.location = location
        self.date = date
        self.decade = decade


class WTImage:
    def __init__(self, url="", width=0, height=0):
        self.url = url
        self.width = width
        self.height = height


class WTRelType(Enum):
    UNKNOWN = 0
    SPOUSE = 1
    CHILD = 2


class WTRelation:
    def __init__(self, rel_type, p1, p2, parent=None):
        self.rel_type = rel_type
        self.p1 = p1
        self.p2 = p2
        self.parent = parent


class WTPrivacy(Enum):
    UNLISTED = 10
    PRIVATE = 20
    SEMIPRIVATE_BIO = 30
    SEMIPRIVATE_TREE = 40
    SEMIPRIVATE_BIOTREE = 50
    OPEN = 60


class WTProfile:
    pass


class WTPerson(WTProfile):
    def __init__(self):
        self.key = ""
        self.id_ = 0
        self.first_name = ""
        self.manager = None


class WTPage(WTProfile):
    pass
