class WTException(Exception):
    pass


class WTCredentialsException(WTException):
    pass


class WTProfileIdentificationException(WTException):
    pass
