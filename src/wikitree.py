import os
import urllib
from collections import namedtuple

import requests

from .dataclasses import WTPerson
from .errors import WTCredentialsException, WTProfileIdentificationException

HOST = "api.wikitree.com"
PORT = 443
PATH = "api.php"
SCHEME = "https"

USER = os.environ.get("WT_USER")
PASS = os.environ.get("WT_PASS")

Credentials = namedtuple("Credentials", ("username", "password", "authcode"))


class WTSimpleAuth:
    def __init__(self, user="", password=""):
        self.set_credentials(user, password)

    def set_credentials(self, user="", password=""):
        """
        Just to be able to set credentials afterwards, not immediatelly during construction
        """
        self.__user = user
        self.__pass = password

    def get_cookies(self):
        query = {
            "action": "clientLogin",
            "doLogin": 1,
            "wpEmail": self.__user,
            "wpPassword": self.__pass,
        }

        base_url = "https://api.wikitree.com:443/api.php"
        url = base_url + "?" + urllib.parse.urlencode(query)

        resp = requests.head(url)

        if not resp.headers.get("location"):
            raise WTCredentialsException("Credentials not set or incorrect")

        authcode = resp.headers["location"].split("=")[1]

        resp = requests.post(base_url, {"action": "clientLogin", "authcode": authcode})

        return dict(resp.cookies)


class Wikitree:
    def __init__(self, authenticator=None, host=HOST, port=PORT, path=PATH, scheme=SCHEME):
        self.__host = host
        self.__port = port
        self.__path = path
        self.__scheme = scheme
        self.__credentials = Credentials("", "", "")
        self.__authenticator = authenticator

        self.__logged_user = ""

        self.__session = requests.Session()

    @property
    def logged_user(self):
        return self.__logged_user

    def __run(self, query, method="GET", format="json"):
        api_base = f"{self.__scheme}://{self.__host}:{self.__port}/{self.__path}?"

        if format:
            query["format"] = format

        query = urllib.parse.urlencode(query)

        url = api_base + query

        print(url)

        if method == "POST":
            resp = self.__session.post(url)
        if method == "HEAD":
            resp = self.__session.head(url)
        else:
            resp = self.__session.get(url)

        return resp

    def login(self):
        if not self.__authenticator:
            return

        cookies = self.__authenticator.get_cookies()

        for c_key, c_value in cookies.items():
            self.__session.cookies[c_key] = c_value

        self.__logged_user = urllib.parse.unquote(cookies["wikidb_wtb_UserName"])

    def logout(self):
        delete = [key for key in dict(self.__session.cookies) if key.startswith("wikidb_wtb_")]
        for key in delete:
            del self.__session.cookies[key]

    def get_person(self, key="", id=0, fields=[]):
        """
        :param key: ID of searched profile
        :param fields:  list of properties that will be listed in result with following possible values: Id, Name, FirstName, MiddleName, MiddleInitial, LastNameAtBirth, LastNameCurrent, Nicknames, LastNameOther, RealName, Prefix, Suffix, Gender, BirthDate, DeathDate, BirthLocation, DeathLocation, BirthDateDecade, DeathDateDecade, Photo, Mother, Father, Parents, Children, Siblings, Spouses, HasChildren, NoChildren, Touched, IsLiving, Privacy, DataStatus, Derived.ShortName, Derived.BirthNamePrivate, Derived.LongNamePrivate, Derived.LongName, Derived.BirthName, Manager

        :return: dict
        """

        if not key and not id:
            raise WTProfileIdentificationException("Either ID or key is required in order to obtain profile data")

        query = {"action": "getPerson", "key": key if key else str(id)}

        if fields:
            query["fields"] = ",".join(fields)

        resp = self.__run(query).json()[0]

        print(resp)

        person = WTPerson()

        if resp["profile"]:
            if resp["profile"].get("Id"):
                person.id = resp["profile"]["Id"]

            if resp["profile"].get("RealName"):
                person.first_name = resp["profile"]["RealName"]

            if "Manager" in fields:
                person.manager = Person(id_=resp["profile"]["Manager"])

        return person

    def get_bio(self, key: str, bio_format=""):
        """
        :param key: ID of searched profile
        :param bio_format: [ wiki | html | both ]: none = wiki
        :return: dict
        """
        query = {"action": "getBio", "key": key}
        if bio_format:
            query["bioFormat"] = bio_format

        return self.__run(query).json()[0]

    def get_watchlist(self):
        """
        :return: dict
        """
        query = {"action": "getWatchlist"}

        return self.__run(query).json()[0]


x = Wikitree(authenticator=WTSimpleAuth(USER, PASS))
# resp = x.get_profile("Vašut-2", ["Name", "FirstName", "LastNameCurrent", "Mother", "Father"])

# resp = x.get_profile("Vašut-2")

# x.get_profile("Vašut-2")

# print(resp)
